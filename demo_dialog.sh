
#! /bin/bash


# foreground colors 
FGblue="\e[38;5;17m"
FGgreen="\e[38;5;40m"
FGpink="\e[38;5;165m"
FGwhite="\e[38;5;255m"
FGred="\e[38;5;196m"
#background colors
BGgreen="\e[48;5;40m"
BGred="\e[48;5;196m"
BGwhite="\e[48;5;255m"
BGgrey="\e[48;5;232m"
razcolor='\e[0m'
# demo of dialog based on dialog programm

# simple message in a box 
# 5 is the height , 20 is the width 

clear


printf "exemple of a simple box with a $FGred red $FGwhite colored word inside \n"
printf $FGgreen
printf $BGgrey
echo "dialog --colors --title 'Message' --msgbox 'Hello, world! in \Z1red\Zn' 8 25"
printf $razcolor
read -p "type <CR> to go on " uselessvar



dialog --colors --title 'Message' --msgbox 'Hello, world! in \Z1red\Zn' 8 25

clear

# message in a box with a question 

printf "exemple of a simple question box with a $FGgreen green $FGwhite colored word inside \n"

printf $FGgreen
printf $BGgrey
echo "dialog --colors --title \"Message\"  --yesno \"Are you having fun? in \Z2green\Zn\" 15 25 "
printf $razcolor

read -p "type <CR> to go on " uselessvar

dialog --colors --title "Message"  --yesno "Are you having fun? in \Z2green\Zn" 15 25

response=$?

case $response in
	1 | 255)
    clear
	echo " you have typed no or esc , have a nice day ... "
	exit 0;;
	0) 
    clear
    printf " you have typed $FGpink $BGgrey yes $razcolor, we go on "
esac

read -p "type <CR> to go on " uselessvar


# menu with a single choice in a list 
# 15 is the height , 40 is the width , 5 is the max number of items in the list you will see on the screen

echo "exemple of a SINGLE choice in a list "

printf $FGgreen
printf $BGgrey
echo "dialog --menu \"Choose one colour :\" 15 40 5 \\"
echo "first red second green third blue 4 cyan  2> tmpinput"
printf $razcolor

dialog --menu "Choose one colour :" 15 40 5 \
first red second green third blue 4 cyan  2> tmpinput

valret=$?
choix=`cat tmpinput`

clear 

case $valret in
 0)	printf $FGpink
    printf $BGgrey
    printf  "$FGpink $BGgrey $choix $razcolor is your favorite colour \n"
    printf $razcolor;;
 1) 	echo "Appuyé sur Annuler.";;
255) 	echo "Appuyé sur Echap.";;
esac

read -p "type <CR> to go on " uselessvar


echo "exemple of a multiple choice in a list "

printf $FGgreen
printf $BGgrey
echo "dialog --checklist \"Choose toppings:\" 10 40 3 \\"
echo "        1 Cheese on \\"
echo "        second \"Tomato Sauce\" on \\"
echo "        option Anchovies off   2> tmpinput "
printf $razcolor

read -p "type <CR> to go on " uselessvar

# exemple of a checklist , the user will check all requested options
dialog --checklist "Choose toppings:" 10 40 3 \
        1 Cheese on \
        second "Tomato Sauce" on \
        option Anchovies off   2> tmpinput

clear

valret=$?
choix=`cat tmpinput`

case $valret in
 0)	printf $FGpink 
    printf $BGgrey
    printf "$FGpink $BGgrey $choix $razcolor is your choice \n"
    printf $razcolor
    echo " list item by item"
    printf $FGpink
    printf $BGgrey
    for item in $choix
    do
    	echo $item
    done 
    printf $razcolor;;

 1) 	echo "Appuyé sur Annuler.";;
255) 	echo "Appuyé sur Echap.";;
esac

read -p "type <CR> to go on " uselessvar

# exemple of a single input
clear

echo "exemple of a simple input "
printf $FGgreen
printf $BGgrey
echo "dialog --inputbox \"your ident\" 10 40 \"Cu_Id\" \\"
echo "       2> tmpinput"
printf $razcolor

dialog --inputbox "your ident" 10 40 "Cu_Id" \
       2> tmpinput

clear

valret=$?
choix=`cat tmpinput`

case $valret in
 0)	 printf "$FGpink $BGgrey $choix $razcolor is your input \n"
     printf $razcolor ;;
 1) 	echo "Appuyé sur Annuler.";;
255) 	echo "Appuyé sur Echap.";;
esac

read -p "type <CR> to go on " uselessvar


clear

echo "exemple of a password input but not convenient at all"
printf $FGgreen
printf $BGgrey
echo "dialog --passwordbox \"your password\" 10 40 \"initial password\" \\"
echo "       2> tmpinput"
printf $razcolor

read -p "type <CR> to go on " uselessvar

# exemple of an nput to get a passwd 
dialog --passwordbox "your password" 10 40 "initial password" \
       2> tmpinput

clear

valret=$?
choix=`cat tmpinput`

case $valret in
 0)	printf "$FGpink $BGgrey $choix $razcolor is your input \n"
    echo "'$choix' is your input"
    printf $razcolor;;
 1) 	echo "Appuyé sur Annuler.";;
255) 	echo "Appuyé sur Echap.";;
esac

read -p "type <CR> to go on " uselessvar

clear


# multiple input , which is very convenient
echo "exemple of a multiple input , very convenient "
printf $FGgreen
printf $BGgrey
echo "dialog --backtitle \"RDT client Setup \" --title \"Credentials and IP - Form\" \\"
echo "--form \"use <tab> and <arrow> to move\" 25 60 10 \\"
echo "\"Username:\" 1 1 \"User\"  1 30 25 0  \\"
echo "\"Password:\" 2 1 \"Pwd\"   2 30 25 0  \\"
echo "\"RDT Server Public ipv4 :\" 3 1 \"RDT_IP\" 3 30 25 0  \\"
echo "\"RDT Server Private ipv4 :\" 4 1 \"RDT_Private_IP\" 4 30 25 0 \\"
echo " 2> tmpform"
printf $razcolor


dialog --backtitle "Multiple Input form " --title "Credentials and IP - Form" \
--form "use <tab> and <arrow> to move" 25 60 10 \
"Username:" 1 1 "User"  1 30 25 0  \
"Password:" 2 1 "Pwd"   2 30 25 0  \
"RDT Server Public ipv4 :" 3 1 "RDT_IP" 3 30 25 0  \
"RDT Server Private ipv4 :" 4 1 "RDT_Private_IP" 4 30 25 0 \
 2> tmpform

clear

valret=$?
choix=`cat tmpform`

echo "full result without processing"
printf $FGpink
printf $BGgrey
echo $choix
printf $razcolor

case $valret in
 0)	echo "this is the list of your input using bash 'for' statement "
    printf $FGpink
    printf $BGgrey
    for item in $choix
    do
    	echo $item
    done 
    printf $razcolor
    echo "convert the string is a bash array and print the second item"
    My_array=($choix)
    # print the second item , the index starts at 0 
    printf "the second field is $FGpink $BGgrey ${My_array[1]} $razcolor \n "
    printf $razcolor ;;
 1) 	echo "Appuyé sur Annuler.";;
255) 	echo "Appuyé sur Echap.";;
esac

read -p "type <CR> to go on " uselessvar

clear

echo "read a file in a box , can be nice"
printf $FGgreen
printf $BGgrey
echo "dialog --backtitle \"dialog --create-rc ~/.dialogrc\" --textbox $HOME/.dialogrc 30 60 "
printf $razcolor
read -p "type <CR> to go on " uselessvar


# file created by dialog --create-rc ~/.dialogrc
# this file is used to manage colours 
# once created, you can modify default colors, the most important parameter is use_colors
dialog --backtitle "dialog --create-rc ~/.dialogrc" --textbox $HOME/.dialogrc 15 60 

clear

dialog --colors --title 'bash dialog quick overview' \
--msgbox "This is the end of this short overview of Dialog \n\
you know how to create more friendly shell scripts \n\
What has been presented should be enough in most situations but do not hesitate to google 'dialog'\n\
\Z1One important thing to remember\Zn : it is not possible to select a text inside a 'box'\n\
Next tool : whiptail provides this feature but it does not allow to assign different colors in a box \
" 20 60

