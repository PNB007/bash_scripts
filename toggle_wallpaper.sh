
#! /bin/bash


if ! [ -f $HOME/bin/freeze_wallpaper ]; then
	

dialog   --colors   --title "FREEZE WALLPAPER" --yesno "Currently, the wallpaper changes each time you login,  \n\n\
If you wish to freeze current wallpaper because you like it,\n\
answer \Z2yes\Zn\n\n\
                  " 10 90

if [ $? = 1 ] ; then
	clear
	echo " bye , have a nice day ... "
	exit 0
fi

touch $HOME/bin/freeze_wallpaper

else

dialog --colors  --title  "UNFREEZE WALLPAPER" --yesno "Currently, the wallpaper is frozen \n\n\
If you wish to unfreeze the wallpaper and gets a new random one at every login \n\
answer \Z2yes\Zn\n\n\
                  " 10 90

if [ $? = 1 ] ; then
	clear
	echo " bye , have a nice day ... "
	exit 0
fi

rm $HOME/bin/freeze_wallpaper	


	#statements
fi




