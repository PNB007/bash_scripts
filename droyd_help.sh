#!/bin/bash

# first , get the latest version number of both appium and droydrunner container

# IMPORTANT : the private TOKEN used to acces -READ ONLY- is belonging to ATAQ team 
# -s option is mandatory to avoid a download 'progress' status which would mess a ittle bit the script output



	    dialog  --colors   --title "Mobile testing Tutorial" --msgbox "   \n\
 If you are interrested in testing Mobiles applications on Mobiles available in the Mobile farm called LaaS \n\
 This help will open few files : \n\
 - a tutorial (based on libreoffice) explaining \n\
      - how to use the Mobile farm through the Web access, \n\
      - the key part of Android Debug \n\
      - and finally how to write RobotFramework scripts based on DroydRunner Library \n\
 - Firefox will open both Uiautomator and Droydrunner library \n\
 - Sublime will open a ADB (Android Debug) help \n\
 Wait for files to be opened and start by the tutorial \n\n\
 Note : DROYDRUNNER is an additional layer wrapping \Z4\ZuUIAUTOMATOR robotframework library\Zn \n\
        Additional features are : \n\
        - client server architecture which enables to deploy your server in a remote location \n\
        - very easy \Z4\Zumultiple mobile phones\Zn handling \n\

                   " 24 90


clear

echo  " "

echo "Wait for libreoffice, Firefox and sublime to open files"

nohup firefox  ~/droydrunner_help/Mobile.html    ~/droydrunner_help/uiautomatorlibrary.html  > /dev/null 2>&1  &

nohup sublime ~/Robot_Project/droyd_scripts/adb_help.txt > /dev/null 2>&1  &

nohup libreoffice ~/Robot_Project/droyd_scripts/Tutorial.odp > /dev/null 2>&1  &

sleep 3

exit 0






