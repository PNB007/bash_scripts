#!/bin/bash

# change language of the bureau 

echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****     SSHD Disable Password Authentication         *****"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"



	whiptail --title "Live SSH security improvement" --yesno "This is Step 4 in the global process to improve SSH security on your 'Live'\n\n\
Before running this step, you must have successfully ran step 1 to 3 \n\
and a private key must be configured on your local computer\n\
This script will modify /etc/ssh/sshd_config to set : \n\
     PasswordAuthentication no \n\
and will restart SSHD process just after \n\
do you want to go on ? : \n\
                  " 20 90


# echo " "
# echo " "
# echo " This is Step 4 in the global process to improve SSH security on your Live desktop "
# echo " before running this script, you must have succceded in login with a secret key    "
# echo " configured during step 1, 2  and 3 "
# echo " "
# echo " "
# echo " WARNING : this script will modify /etc/ssh/sshd_config to set : "
# echo "           PasswordAuthentication no "
# echo "           and will just afer restart SSHD service  "
# echo " "
# echo " "

# read  -p "      do you still want to continue (y/n default) : "  l_input
# l_input="${l_input:-"n"}"

if [ $? = 0 ] ; then
	sudo sed -i -e "s/^PasswordAuthentication *\(yes\|no\) *$/PasswordAuthentication no/g" /etc/ssh/sshd_config
	echo "           SSHD config has been modified "
	sudo service sshd restart
	echo "           Great !! "
	echo "           SSH access to Live is now secured by a private key !! "
	exit 0
fi

	echo "     Too bad ...  "
	echo "     Think about it again since keeping classic password authentication is a weak protection "
	echo "     bye "
	exit 1

