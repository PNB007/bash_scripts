#!/bin/bash

# first , get the latest version number of both appium and droydrunner container

# IMPORTANT : the private TOKEN used to acces -READ ONLY- is belonging to ATAQ team 
# -s option is mandatory to avoid a download 'progress' status which would mess a ittle bit the script output

echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****            Portainer GUI installation            *****"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"


echo "volume creation portainer_data"
docker volume create portainer_data

sleep 1

if docker ps -a --format '{{.Names}}' | grep -q 'portainer' ; then
	echo "\ncontainer portainer already exists locally"
	echo "stop container if running .... and then delete it" 
	docker stop portainer >/dev/null
	docker rm portainer
fi

echo "docker portainer container installation"
if docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer ; then


	whiptail --title "PORTAINER SETUP" --msgbox "\n\n  !!  CONGRATULATIONS  !! \n\n\
 PORTAINER container is now running  \n\n\
  - use  http://127.0.0.1:9000 \n\n" 12 90
	# echo "************************************************************"
	# echo "************************************************************"
	# echo "*****                                                  *****"
	# echo "*****                                                  *****"
	# echo "*****               installation done                  *****"
	# echo "*****           use http://127.0.0.1:9000              *****"
	# echo "*****                                                  *****"
	# echo "************************************************************"
	# echo "************************************************************"
else

		whiptail --title "PORTAINER SETUP" --msgbox "\n\n  !!  WARNING  !! \n\n\
 PORTAINER container installation FAILED ?  \n\n\
 Have you configured CNTLM (if you are in the RSC) ?\n\n" 12 90
	# echo "************************************************************"
	# echo "************************************************************"
	# echo "*****                                                  *****"
	# echo "*****                                                  *****"
	# echo "*****               installation FAILED                *****"
	# echo "*****            have you configured CNTLM ?           *****"
	# echo "*****                                                  *****"
	# echo "************************************************************"
	# echo "************************************************************"
fi






# laas_version=$(curl -s --request GET --header 'PRIVATE-TOKEN: Yioqks7dM4smYeSQc1W8' 'https://gitlab.forge.orange-labs.fr/devops-store/AtaqTools/raw/master/laas_docker_version.json') 


# laas_version_string_size=$(echo $laas_version | wc -c )



# if [ laas_version != .*laas.*laas.*  ]; then
# 	#statements
# 	echo "there is an issue with devops store access : "
# 	echo $laas_version
# 	sleep 2
# 	droydversion="1.1"
# 	appiumversion="1.1"
# 	read  -p "Please enter droydrunner version (default : $droydversion) : "  l_input
# 	droydversion="${l_input:-$droydversion}"

# else
# 	# jq is a dedicated tool which parses json string , finally, strip the "
# 	droydversion=$(echo $laas_version | jq '.laas_droydrunner' | sed -e 's/"//g')
# 	appiumversion=$(echo $laas_version | jq '.laas_appium'  | sed -e 's/"//g')
# fi


# droydURL="dockerfactory-iva.si.francetelecom.fr/laas_droydrunner:$droydversion"
# appiumURL="dockerfactory-iva.si.francetelecom.fr/laas_appium:$appiumversion"

# echo "droyd URL : "$droydURL
# echo "appium URL : "$appiumURL

# sleep 2



# if docker ps -a --format '{{.Names}}' | grep -q 'droydrunner' ; then
# 	echo "\ncontainer droydrunner exists locally"
# 	echo "stop container if running .... and then delete it" 
# 	docker stop droydrunner >/dev/null
# 	docker rm droydrunner
# fi

# echo "\ndownload latest image version if needed ....."
# docker pull $droydURL
# echo "\nstart droydrunner container"
# echo "\n        container will start tcp port 5200 for remote Laas Library "
# echo "\n        container will start tcp port 5201 for droydrunner server entry point "
# docker run -d --name droydrunner -p 5200:5000 -p 5201:5001 --privileged -v /dev/bus/usb:/dev/bus/usb $droydURL
# # erase images which are no more associated with a container
# docker image prune -f



# echo "\n\nusefull docker related commands : "
# echo "    docker start <container name> " 
# echo "    docker stop  <container name> " 
# echo "    docker ps    (list running container) " 
# echo "    docker ps -a (list all containers running or not) " 