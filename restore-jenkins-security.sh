#!/bin/bash

# change language of the bureau 

echo "***************************************************************"
echo "***************************************************************"
echo "*****                                                     *****"
echo "*****                                                     *****"
echo "*****            JENKINS Security restore                 *****"
echo "*****                                                     *****"
echo "*****    This script is mandatory if you want to          *****" 
echo "*****         use Live as a Jenkins Server                *****"
echo "*****     which can be accessed by remote users           *****"
echo "*****     IMPORTANT : do not forget to update the         *****"
echo "*****                 Security Group if you run in a cloud*****"
echo "*****     Current configuration : no user and no          *****"
echo "*****     authentification but access is limited to       *****"
echo "*****     localhost                                       *****"
echo "*****                                                     *****"
echo "***************************************************************"
echo "***************************************************************"
echo " "
echo " "
echo " "

# basically, things to do are very easy : restore the initial xml config file and restart jenkins

echo " Restoring initial /var/lib/jenkins/config.xml file "
cp /var/lib/jenkins/config.save.xml /var/lib/jenkins/config.xml 


sudo cat /etc/default/jenkins | sed -e 's/Djenkins.install.runSetupWizard=false/Djenkins.install.runSetupWizard=true/g' > /home/live/bin/jenkins.tmp
sudo cp /home/live/bin/jenkins.tmp /etc/default/jenkins

echo " Restarting Jenkins service ..... "
sudo service jenkins restart
echo " "
echo " "
echo " "
echo "***************************************************************"
echo "***************************************************************"
echo "*****                                                     *****"
echo "*****                                                     *****"
echo "*****    Done !! JENKINS Security has been restored       *****"
echo "*****                                                     *****"
echo "*****    access jenkins with http://127.0.0.1:8080        *****"
echo "*****    or with the shortcut on your desktop             *****" 
echo "*****                                                     *****"
echo "*****                                                     *****"
echo "***************************************************************"
echo "***************************************************************"
echo " "
echo " "


