#!/bin/bash

# change language of the bureau 

echo "***************************************************************"
echo "***************************************************************"
echo "*****                                                     *****"
echo "*****                                                     *****"
echo "*****            JENKINS Security disable                 *****"
echo "*****                                                     *****"
echo "*****    WARNING : usefull if you are the only one        *****" 
echo "*****         accessing the 'live' and using Jenkins      *****"
echo "*****    It will ease initial configuration and setup     *****"
echo "*****                                                     *****"
echo "*****                                                     *****"
echo "***************************************************************"
echo "***************************************************************"
echo " "
echo " "
echo " "

# basically, things to do are very easy : restore the initial xml config file and restart jenkins

echo " Restoring initial /var/lib/jenkins/config.xml file "
cp /var/lib/jenkins/config.save.xml /var/lib/jenkins/config.xml 
echo " Modifying  /var/lib/jenkins/config.xml file to change <useSecurity>true</useSecurity>  to false "
cat /var/lib/jenkins/config.xml | sed -e 's/<useSecurity>true<\/useSecurity>/<useSecurity>false<\/useSecurity>/g' > /var/lib/jenkins/config.unsafe.xml
sed '/.*authorizationStrategy.*/d'   /var/lib/jenkins/config.unsafe.xml >  /var/lib/jenkins/config.unsafe.xml.tmp
sed '/.*securityRealm.*/d'   /var/lib/jenkins/config.unsafe.xml.tmp >  /var/lib/jenkins/config.unsafe.xml
cp /var/lib/jenkins/config.unsafe.xml  /var/lib/jenkins/config.xml 


sudo cat /etc/default/jenkins | sed -e 's/Djenkins.install.runSetupWizard=true/Djenkins.install.runSetupWizard=false/g' > /home/live/bin/jenkins.tmp
sudo cp /home/live/bin/jenkins.tmp /etc/default/jenkins

echo " Restarting Jenkins service ..... "
sudo service jenkins restart
echo " "
echo " "
echo " "
echo "***************************************************************"
echo "***************************************************************"
echo "*****                                                     *****"
echo "*****                                                     *****"
echo "*****    Done !! JENKINS Security has been disabled       *****"
echo "*****                                                     *****"
echo "*****    access jenkins with http://127.0.0.1:8080        *****"
echo "*****    or with the shortcut on your desktop             *****" 
echo "*****                                                     *****"
echo "*****                                                     *****"
echo "***************************************************************"
echo "***************************************************************"
echo " "
echo " "


