#!/bin/bash

# change language of the bureau 


ESR_version=$(firefox-esr -v)
Firefox_version=$(\firefox -v)

grep_esr_in_alias=$(grep 'firefox-esr' /home/live/.bash_aliases)

if  [ -z "$grep_esr_in_alias" ] ; then

	Current_version="$Firefox_version"

 dialog --title "Firefox Version Selection" \
--backtitle "Firefox Setup" --colors \
--yesno "This setup program let you choose wich firefox version you want to run\n\
 You may use 2 different versions :\n\
 \Z2$ESR_version \Zn\n\
     or \n\
 \Z4$Firefox_version\Zn\n\
 Currently, the system is running the latest available version for ubuntu : \Z4\Zu$Current_version\Zn\n\
 Do you want to switch to the ESR version ? \n\
                   " 15 80
 response=$?



else
	Current_version="$ESR_version"

 dialog --title "Firefox Version Selection" \
--backtitle "Firefox Setup" --colors \
--yesno "This setup program let you choose wich firefox version you want to run\n\
 You may use 2 different versions :\n\
 \Z2$ESR_version \Zn\n\
     or \n\
 \Z4$Firefox_version\Zn\n\
 Currently, the system is running the ESR version : \Z2\Zu$Current_version\Zn\n\
 Do you want to switch to the latest available version ? \n\
                   " 15 80
 response=$?

	#statements
fi





case $response in
	1 | 255)
    clear
	echo " bye , have a nice day ... "
	exit 0
esac

clear

sleep 2


if [ -z "$grep_esr_in_alias" ] ; then
	echo "switch to the Extended Support Release"
	# alias firefox='firefox-esr'
	sed -i -e "s/^alias firefox.*/alias firefox='firefox-esr'/g" /home/live/.bash_aliases

else
	echo "switch to the latest available release"
	# alias firefox='firefox'
	sed -i -e "s/^alias firefox.*/alias firefox='firefox'/g" /home/live/.bash_aliases
fi


echo "The switch has been done , check it by running 'alias | grep firefox ' command "

# gnome-session-quit 








