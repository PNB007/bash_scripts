#!/bin/bash


# change language of the bureau 




echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****      SSHD Enable Password Authentication         *****"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"


echo " "
echo " "
echo " "
echo " WARNING : this script will modify /etc/ssh/sshd_config to set : "
echo "           PasswordAuthentication yes "
echo "           and will just afer restart SSHD service "
echo "           it should not close your current SSH connexion"
echo "           On a security point of view, "
echo "           this is a DANGEROUS configuration especially if you are connected to the WWW "
echo " "
echo " "
echo " "

read  -p "      do you still want to continue (y/n default) : "  l_input
l_input="${l_input:-"n"}"

if [ $l_input = "y" ] ; then

	sudo sed -i -e "s/^PasswordAuthentication *\(yes\|no\) *$/PasswordAuthentication yes/g" /etc/ssh/sshd_config
	echo "           SSHD config has been modified accordingly , the service is going to restart "
	sudo service sshd restart

fi





