#!/bin/bash

# first , get the latest version number of both appium and droydrunner container

# IMPORTANT : the private TOKEN used to acces -READ ONLY- is belonging to ATAQ team 
# -s option is mandatory to avoid a download 'progress' status which would mess a ittle bit the script output

ESR_version=$(firefox-esr -v)
Firefox_version=$(\firefox -v)
Current_version=$(firefox -v)


	    dialog  --colors --ok-label "Next"  --title "README" --msgbox "   \n\
 Here is a summary of what is new in this release of the Live desktop \n\
 This is version \Z2\Zu413\Zn of the 'Live' \n\
 What's new ? : \n\
 \Zu\Z4Selenium web testing :\Zn \n\
    * This platform is running the last available Selenium end Geckodriver version \n\
    to be able to perform RobotFramework tests with the last Firefox and Chrome \n_
    versions LOCALLY on your computer \n\
    However, the last Selenium version is not yet implemented on the Grid Selenium.\n\
    This has an unfortunate consequence on 'remote' Robot Framework tests on the Grid :\n\
         - it works with any browser : Chrome, Edge, IE but \Z4NOT with Fiefox\Zn\n\
          If testing Firefox on the Grid is mandatory, you must choose a prevous version of 'Live'\n\
          or wait for the upgrade of the Grid Selenium \n\
    * this platform is running 2 different versions of Firefox :\n\
        - the ESR (Extended Support Release) version : \Z1$ESR_version\Zn\n\
        - and the last version for ubuntu 18.04 :  \Z1$Firefox_version\Zn \n\
        ESR is the default running version but you can change with the tool \n\
        \ZuFirefox Version Selection\Zn\n\
                   " 26 92


clear




	    dialog  --colors   --title "README" --msgbox "   \n\
 \Zu\Z4Mobile Testing with LaaS :\Zn  \n\
  There is a tutorial explaining \n\
     - how to use the \Z2LaaS Mobile Farm\Zn \n\
     - the basics of \Z2Android Debug\Zn\n\
     - and how to use \Z2uiautomator and DroydRunner\Zn to write RF scripts\n\
  There are also additional informations : \n\
     - a file with a summary of most important \Z2Android Debug\Zn commands \n\
     - the description of \Z2Droydrunner\Zn and \Z2uiautomator\Zn RobotFramework libraries \n\
 \ZuNew Linux related tools :\Zn \n\
 - \Z2tmux\Zn is a very convenient linux terminal multiplexer which let you handle several windows inside the same terminal window \n\
   just click on the 'icon' and you will have a quick tutorial to learn how to use it \n\
 - \Z2sublime\Zn is a text editor which can be configured to understand '.robot' files with completion and other features \n\
   just click on the icon to launch the setup \n\

                   " 26 90


exit 0






