#!/bin/bash

# change language of the bureau 




echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****                 TMUX launch                      *****"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"




tmux new-session -s 'Live' \; \
 rename-window 'life' \;\
 split-window -v \; \
 send-keys 'fortune' C-m \; \
 split-window -h \; \
 send-keys 'date' C-m \; \
 new-window \;\
 rename-window 'is cool' \;\
 next-window \;




