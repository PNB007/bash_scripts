#!/bin/bash

# first , get the latest version number of both appium and droydrunner container

# IMPORTANT : the private TOKEN used to acces -READ ONLY- is belonging to ATAQ team 
# -s option is mandatory to avoid a download 'progress' status which would mess a ittle bit the script output

laas_version=$(curl -s --request GET --header 'PRIVATE-TOKEN: Yioqks7dM4smYeSQc1W8' 'https://gitlab.forge.orange-labs.fr/devops-store/AtaqTools/raw/master/laas_docker_version.json') 

laas_version_string_size=$(echo $laas_version | wc -c )

# set default version to 1.1


# if [ laas_version != .*laas.*laas.*  ]; then
# 	#statements
# 	echo "there is an issue with devops store access : "
# 	echo $laas_version
# 	sleep 1
# 	droydversion="1.2"
# 	appiumversion="1.3"
# 	read  -p "Please check devops store to enter last available appium version (default : $appiumversion) : "  l_input
# 	appiumversion="${l_input:-$appiumversion}"
# else
# 	# jq is a dedicated tool which parses json string , finally, strip the "
# 	droydversion=$(echo $laas_version | jq '.laas_droydrunner' | sed -e 's/"//g')
# 	appiumversion=$(echo $laas_version | jq '.laas_appium'  | sed -e 's/"//g')
# fi




if [ laas_version != .*laas.*laas.*  ]; then
	#statements
	# echo "there is an issue with devops store access : "
	# echo $laas_version
	sleep 1
	droydversion="1.4"
	appiumversion="1.6"

	dialog --backtitle "APPIUM Version selection " --title "Appium Version Selection - Form" \
    --form "use <tab> or <arrow> to move" 12 60 5 \
"Appium Version (check DevopsStore):" 1 1 $appiumversion  1 40 6 6  > /home/$USER/bin/form.tmp \
    2>&1 >/dev/tty

    exitcode=$?


    if [ $exitcode = 1 ] ; then
	    clear
	    echo " bye , have a nice day ... "
	    exit 0
    fi

    appiumversion=`sed -n 1p /home/$USER/bin/form.tmp`


    if [[ ! $appiumversion =~ [0-9].[0-9] ]] ; then 
    	clear
		echo "Appium version syntax invalid"
		echo "it should be <digit>.<digit>"
		echo "bye"
		exit 0	
	fi 


	# read  -p "Please check devops store to enter last available droydrunner version (default : $droydversion) : " l_input
	# droydversion="${l_input:-$droydversion}"
	

else
	# jq is a dedicated tool which parses json string , finally, strip the "
	droydversion=$(echo $laas_version | jq '.laas_droydrunner' | sed -e 's/"//g')
	appiumversion=$(echo $laas_version | jq '.laas_appium'  | sed -e 's/"//g')
fi

clear



droydURL="dockerfactory-cwfr1.rd.francetelecom.fr/laas_droydrunner:$droydversion"
appiumURL="dockerfactory-cwfr1.rd.francetelecom.fr/laas_appium:$appiumversion"

# echo "droyd URL : "$droydURL
echo " "
echo "appium container URL : "$appiumURL
echo " "

sleep 1

if docker ps -a --format '{{.Names}}' | grep -q 'appium' ; then
	echo "\ncontainer appium exists locally"
	echo "stop container if running .... and then delete it" 
	docker stop appium >/dev/null
	docker rm appium
fi

echo "download latest version if needed ....."
docker pull $appiumURL
echo "start appium container"
echo "        container will start tcp port 6000 for remote Laas Library "
echo "        container will start tcp port 6001-6100 for appium server entry point "
docker run -d --name appium -p 6000:6000 -p 6001-6100:6001-6100 --privileged -v /dev/bus/usb:/dev/bus/usb $appiumURL
# erase images which are no more associated with a container
docker image prune -f


if docker ps --format '{{.Names}}' | grep -q 'appium' ; then

    dialog --colors --title "APPIUM SETUP" --msgbox "  \Z2!!  CONGRATULATIONS  !!\Zn \n\
 APPIUM container is now running  \n\n\
  - tcp port \Zu6000\Zn is used to access remote Laas Library \n\
  - tcp port \Zu6001-6100\Zn are used as appium server entry point \n\n\
                   " 15 90


	# echo "***************************************************"
	# echo "***************************************************"
	# echo "****             CONGRATULATIONS              *****"
	# echo "****     container appium is now running      *****"
	# echo "***************************************************"
	# echo "***************************************************"
else

	    whiptail --title "APPIUM SETUP" --msgbox "    \Z2!! WARNING  !!\Zn \n\
 APPIUM container launch has failed   \n\n\
 The root cause may come from already used ports \n\
 or it may come from a wrong version number\n\
 " 15 90
	# echo "***************************************************"
	# echo "***************************************************"
	# echo "****             WARNING                      *****"
	# echo "****    container appium launch has failed    *****"
	# echo "***************************************************"
	# echo "***************************************************"
	# echo "                                                   "
	echo " netstat results  "
	sudo netstat -tunlep | grep LISTEN | awk '{print $4  "     "  $9}'
	exit 0
fi


	    whiptail --title "Docker Commands" --msgbox "INFORMATION : usefull docker related commands : \n\
    docker start <container name>\n\
    docker stop <container name>\n\
    docker ps    (list running container)\n\
    docker ps -a (list all containers running or not)\n\n\
    docker exec -it <container name> <command> \n\
    but you can also use 'portainer' which is very simple\n\
	   " 14 70



# echo "\n\nusefull docker related commands : "
# echo "    docker start <container name> " 
# echo "    docker stop  <container name> " 
# echo "    docker ps    (list running container) " 
# echo "    docker ps -a (list all containers running or not) " 






