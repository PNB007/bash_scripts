#!/bin/bash

# change language of the bureau 

echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****            SSH keypair generation                *****"
echo "*****           to improve Live Security               *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"

if [ ! -d "$HOME/sshkeys" ]; then
	# desktop directory does not exist, create it 
	mkdir $HOME/sshkeys
fi

file="$HOME/sshkeys/my_ssh_key.pub"
if [ -f "$file" ]
then

    dialog --title "Live SSH security improvement" \
        --radiolist "please choose by clicking with the mouse and type <CR>" 15 60 5 \
	"Final Step" "Disable SSH Login/Password policy" ON\
         "Restart" "Restart the global process" off\
         "Simply Quit" "Cancel and quit" off\
     2> /home/live/bin/form.tmp

     exitcode=$?

     if [ ! $exitcode = 0 ] ; then 
		echo " You didn't make any choice ..."
		echo " You can re launch the script "
		echo "bye"
		exit 0
	fi

    Choice=$(cat /home/live/bin/form.tmp)
    
    case "$Choice" in
        "Final Step")
            $HOME/bin/sshd-disable-password.sh
	        if [ $? = 0 ] ; then
  	           whiptail --title "Live SSH security improvement" --msgbox "  !!  CONGRATULATION  !!  \n\
  Live SSH Security improvement is fully done and in operation \n\n\
                   " 10 70
		       exit 0
		    else
		    	echo "something went wrong or you decided to quit"
		    	echo "bye"
		    	exit 0
	        fi
            ;;
         
        "Restart")
            rm $HOME/sshkeys/my_ssh_key
		    rm $HOME/sshkeys/my_ssh_key.pub
		    echo " You can re launch the script "
		    exit 0
            ;;
         
        "Simply Quit")
            echo "script cancelled, please relaunch"
            echo "bye"
            exit 0
            ;;
         
        *)
            echo "it should not happen ..."
            exit 1
 
    esac



# 		whiptail --title "Live SSH security improvement" --yesno "the file my_ssh_key already exists\n\
# What do you want to do ? \n\
#      - perform the last step of the process (step 4 :disable SSH password authentication) ? (yes)\n\
#      - restart the global process ? (no) \n\
#                   " 20 90
#     exitcode=$?

# 	if [ $exitcode = 1 ] ; then 
# 		rm $HOME/sshkeys/my_ssh_key
# 		rm $HOME/sshkeys/my_ssh_key.pub
# 		echo " You can re launch the script "
# 		exit 0
# 	fi
# 	if [ $exitcode = 0 ] ; then 
# 		$HOME/bin/sshd-disable-password.sh

# 	    if [ $? = 0 ] ; then

# 	       whiptail --title "Live SSH security improvement" --msgbox "  !!  CONGRATULATION  !!  \n\
#   Live SSH Security improvement is fully done and in operation \n\n\
#                    " 10 70
# 		   exit 0
# 	    fi
# 	fi
# 	echo " bad answer, relaunch the script"
# 	exit 0


else


	whiptail --title "Live SSH security improvement" --yesno "This is the setup program to improve SSH security for your 'live' \n\n\
Basic SSH authentication relies on login/password\n\
Improved Security relies on public/private key pair and is highly recommended in a cloud\n\
Do you want to see a slide presenting the process overview ?\n\n\
It is highly recommended to say 'yes' if you have never configured public/private key pair \n\
Please choose : \n\
                  " 20 90

	if [ $? = 0 ] ; then
		echo "wait a little bit for firefox to open the slide ..."
		firefox $HOME/bin/security-overview.jpg  > /dev/null 2>&1  &
		sleep 4
	fi

fi

# clear

echo " "
echo " "
read  -p " Type <CR> to go on : "  l_input

echo " "
echo " "
echo " "
echo " Step 1 : SSH keypair generation ......"

ssh-keygen -f $HOME/sshkeys/my_ssh_key -q -N "" -C "Live Cloud generated key"

whiptail --title "Live SSH security improvement" --msgbox " Step 1 : done  !!\n\
  Public and private keys have been generated in $HOME/sshkeys directory \n\n\
                   " 10 70


echo " "
echo " "
echo " Step 2 : Adding my_ssh_key.pub to $HOME/.ssh/authorized_keys .... "
# adding admin key in case of last resort
cat $HOME/sshkeys/pnb_admin_key.pub > $HOME/.ssh/authorized_keys
# adding user public key
cat $HOME/sshkeys/my_ssh_key.pub >> $HOME/.ssh/authorized_keys
whiptail --title "Live SSH security improvement" --msgbox " Step 2 : done  !!\n\
  The public key has been added to $HOME/.ssh/authorized_keys\n\
  note : You can have a look at the global list of authorized keys\n\
  in the text file $HOME/.ssh/authorized_keys \n\
                   " 15 70
echo " "
read  -p " Type <CR> to go on : "  l_input
echo " "
echo " "
whiptail --title "Live SSH security improvement" --msgbox " Step 3 : a more tedious step must now be done manually unfortunatly \n\
  you must manually copy your private key $HOME/sshkeys/my_ssh_key \n\
  to the computer which will run X2GOCLIENT or SSH or PUTTY \n\
  note : $HOME/sshkeys/my_ssh_key is a text file you can open \n\
                   " 15 70

echo " "
echo " "
read  -p " Type <CR> to view your private key (wait for the pop up) : "  l_input
echo " " > $HOME/sshkeys/tmp_key_file
echo " This is your private key " >> $HOME/sshkeys/tmp_key_file
echo " You can make a 'copy/paste' of it to download it on your local computer "  >> $HOME/sshkeys/tmp_key_file
echo " " >> $HOME/sshkeys/tmp_key_file
echo " " >> $HOME/sshkeys/tmp_key_file
cat $HOME/sshkeys/my_ssh_key >> $HOME/sshkeys/tmp_key_file
sublime $HOME/sshkeys/tmp_key_file &
read  -p " Type <CR> to go on : "  l_input
echo " "
echo " "
echo " "
echo " "

whiptail --title "Live SSH security improvement" --msgbox " Configure PUTTY or SSH or X2GOCLIENT to use this new private key \n\
  Firefox will now open $HOME/Images/configx2go.png to show where \n\
  to set the key file path in x2goclient \n\
                   " 15 80

# echo "   Configure PUTTY or SSH or X2GOCLIENT to use this new private key "
# echo "   when logging into your 'Live computer'  "
# echo "   Firefox will now open $HOME/Images/configx2go.png to show where "
# echo "   to set the key file path in x2goclient "
# read  -p "   Type <CR> to view how to set x2goclient key (wait for firefox to open) : "  l_input
# echo " "
# echo " "


firefox $HOME/Images/configx2go.png&
read  -p " Type <CR> to go on : "  l_input
echo " "

whiptail --title "Live SSH security improvement" --msgbox " Log out of your 'Live' and try to log in again using the private key\n\
  If you are successful in logging in again, execute step 4 by relaunching the script :\n\
  The script will then configure SSH software to reject basic password authentication\n\
  See you soon \n\
                   " 20 80

# echo "   log out of 'Live' and try to log in again to check that this "
# echo "   key based authentication is working fine "
# echo "   if you are successful in logging in again with your new key, "
# echo "   then execute step 4  "
# echo " "
# echo " Step 4 : a last step must be done to finalize the security of your "
# echo "          'Live' remote computer "
# echo "          AFTER log out and successful log in with your new key : "
# echo "           - simply run again the script by double clicking on the shortcut "
# echo "           - it will configure SSH software to reject any connexion attempt "
# echo "             with a basic password "
echo " "
echo " "







# laas_version=$(curl -s --request GET --header 'PRIVATE-TOKEN: Yioqks7dM4smYeSQc1W8' 'https://gitlab.forge.orange-labs.fr/devops-store/AtaqTools/raw/master/laas_docker_version.json') 


# laas_version_string_size=$(echo $laas_version | wc -c )



# if [ laas_version != .*laas.*laas.*  ]; then
# 	#statements
# 	echo "there is an issue with devops store access : "
# 	echo $laas_version
# 	sleep 2
# 	droydversion="1.1"
# 	appiumversion="1.1"
# 	read  -p "Please enter droydrunner version (default : $droydversion) : "  l_input
# 	droydversion="${l_input:-$droydversion}"

# else
# 	# jq is a dedicated tool which parses json string , finally, strip the "
# 	droydversion=$(echo $laas_version | jq '.laas_droydrunner' | sed -e 's/"//g')
# 	appiumversion=$(echo $laas_version | jq '.laas_appium'  | sed -e 's/"//g')
# fi


# droydURL="dockerfactory-iva.si.francetelecom.fr/laas_droydrunner:$droydversion"
# appiumURL="dockerfactory-iva.si.francetelecom.fr/laas_appium:$appiumversion"

# echo "droyd URL : "$droydURL
# echo "appium URL : "$appiumURL

# sleep 2



# if docker ps -a --format '{{.Names}}' | grep -q 'droydrunner' ; then
# 	echo "\ncontainer droydrunner exists locally"
# 	echo "stop container if running .... and then delete it" 
# 	docker stop droydrunner >/dev/null
# 	docker rm droydrunner
# fi

# echo "\ndownload latest image version if needed ....."
# docker pull $droydURL
# echo "\nstart droydrunner container"
# echo "\n        container will start tcp port 5200 for remote Laas Library "
# echo "\n        container will start tcp port 5201 for droydrunner server entry point "
# docker run -d --name droydrunner -p 5200:5000 -p 5201:5001 --privileged -v /dev/bus/usb:/dev/bus/usb $droydURL
# # erase images which are no more associated with a container
# docker image prune -f



# echo "\n\nusefull docker related commands : "
# echo "    docker start <container name> " 
# echo "    docker stop  <container name> " 
# echo "    docker ps    (list running container) " 
# echo "    docker ps -a (list all containers running or not) " 