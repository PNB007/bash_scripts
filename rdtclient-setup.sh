#!/bin/bash

# change language of the bureau 




echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****            WWW RDT client setup                  *****"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"


whiptail --title "WWW RDT CLIENT SETUP" --yesno "This is the setup program to configure the OPENVPN CLIENT to access RDT services over the WWW \n\n\
This SETUP program is useless if you are running your 'Live' in the RSC \n\n\
3 main parameters must be configured : \n\
 - your credentials (user and password) which must have been configured in the RDT server\n\
 - the PUBLIC  IP address of the RDT server\n\
 - the PRIVATE IP address of the RDT server\n\
 note : the private IP is the IP inside the OPENVPN\n
 WARNING:\n\
 Your 'Live' workstation must have access to the WWW, be sure that UDP port 1194 is open on your Firewall\n\
 Do you want to go on ?\n\
                  " 22 90

if [ $? = 1 ] ; then
	clear
	echo " bye , have a nice day ... "
	exit 0
fi

echo " stopping openvpn service ..."
sudo service openvpn stop
echo " done !"

sleep 1

echo "launching form "

nb_line=$(wc -l /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/user-pass  | awk '{ print $1 }')

if [ $nb_line = 2 ] ; then
    Current_User=`sed -n 1p /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/user-pass`
    Current_Pwd=`sed -n 2p /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/user-pass`
else
    Current_User="XXX"
    Current_Pwd="YYY"
fi


if [ ! -f /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/client.conf ]; then
    cp /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/client.ovpn /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/client.conf
    mv /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/client.ovpn /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/client.save
fi

if [ ! -f /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/rdt_private_ip.txt ]; then
	RDT_Private_IP="192.168.X.Y"
else
	RDT_Private_IP=$(cat /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/rdt_private_ip.txt)
fi



RDT_IP=$(grep '^remote' /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/client.conf | awk '{ print $2 }' )

if [ -z "$RDT_IP" ] ; then
    RDT_IP=A.B.C.D
fi

# echo $RDT_IP

sleep 1


dialog --backtitle "RDT client Setup " --title "Credentials and IP - Form" \
--form "use <tab> and <arrow> to move" 25 60 16 \
"Username:" 1 1 $Current_User  1 25 25 30  \
"Password:" 2 1 $Current_Pwd 2 25 25 30  \
"RDT Server Public ipv4 :" 3 1 $RDT_IP 3 25 25 30  \
"RDT Server Private ipv4 :" 4 1 $RDT_Private_IP 4 25 25 30  > /home/$USER/bin/rdt/form.tmp \
2>&1 >/dev/tty

exitcode=$?

clear

if [ $exitcode = 1 ] ; then
	clear
	echo " bye , have a nice day ... "
	exit 0
fi

RDT_User=`sed -n 1p /home/$USER/bin/rdt/form.tmp`
RDT_Password=`sed -n 2p /home/$USER/bin/rdt/form.tmp`
RDT_IP=`sed -n 3p /home/$USER/bin/rdt/form.tmp`
RDT_Private_IP=`sed -n 4p /home/$USER/bin/rdt/form.tmp`


if [ -z "$RDT_User" ]  ; then
	echo "RDT user not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

if [ -z "$RDT_Password" ]  ; then
	echo "RDT Password not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

if [ -z "$RDT_IP" ]  ; then
	echo "RDT Server IP not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

if [ -z "$RDT_Private_IP" ]  ; then
	echo "RDT Server Private IP not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

# if [[ $RDT_IP =~ [0-9]{1-3}.[0-9]{1-3}.[0-9]{1-3}.[0-9]{1-3} ]] ; then 

if [[ ! $RDT_IP =~ [0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3} ]] ; then 
	echo "IP address invalid"
	echo "bye"
	exit 0	
fi 

# substiture '.' with a 'space' 
Liste="${RDT_IP//\./ }"

for Digit in $Liste; do
	if [ $Digit -gt 255 ]; then
		echo 'IP address invalid : ? $Digit ? in  $RDT_IP'
		echo "bye"
		exit 0
	fi
done


rm /home/$USER/bin/rdt/form.tmp

echo $RDT_Private_IP > /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/rdt_private_ip.txt

echo $RDT_User    >   /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/user-pass
echo $RDT_Password  >> /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/user-pass

sed -e "s/;\?remote .*1194$/remote $RDT_IP 1194/" /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/client.conf  >  /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/client.conf.tmp
cp  /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/client.conf.tmp /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/client.conf

# this is just to have a backup
sudo cp /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/*.* /etc/openvpn/client/.
sudo cp /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/user* /etc/openvpn/client/.

# the config files are expected in /etc/openvpn
sudo cp /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/*.* /etc/openvpn/.
sudo cp /home/$USER/bin/rdt/client-vpn-rdt-1.4.0/user* /etc/openvpn/.

clear

echo " restarting openvpn@client ..."
sudo service openvpn@client start
echo " done !"

echo "bye, have a nice day"


whiptail --title "WWW RDT CLIENT SETUP" --msgbox "You should be connected to the OPENVPN managed by the RDT controler \n\n\
TECHNICAL INFORMATION : \n\
all configuration files are in /etc/openvpn \n\
openvpn sends its logs to /var/log/syslog, there is no specific logfile\n\
when you exit, ifconfig will be run, it should show the new tunnel interface\n\
you should also be able to ping the internal RDT server IP $RDT_Private_IP \n\
        " 20 90

sudo ifconfig

ping -c 5 $RDT_Private_IP &





