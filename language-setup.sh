#!/bin/bash

# change language of the bureau 

echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****            language setup                        *****"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"


if grep "fr_FR.UTF-8"  /etc/default/locale ;  then
	echo     " la langue du bureau est FR  (desktop language is french) "
	echo     " voulez vous changer pour l'anglais (do you want to switch to english) ? "
	read  -p "                                                     your choice (y/n) : "  l_input
	l_input="${l_input:-"n"}"

	if [ $l_input = "y" ] ; then
		sudo /usr/sbin/update-locale LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 LANGUAGE=en_US
		echo "*****************************************************************"
		echo "*****  forced logout in 10 secondes                         *****"
		echo "*****                                                       *****"
		echo "*****  IMPORTANT : Please read                              *****"
		echo "*****                                                       *****"
		echo "*****  when relogging, answer YES to directory names update *****"
		echo "*****                                                       *****"
		echo "*****  IMPORTANT                                            *****"
		echo "*****************************************************************"

		## it is important to bear in mind that directory name will change and especially Bureau will become Desktop
		if [ ! -d "$HOME/Desktop" ]; then
			# desktop directory does not exist, create it 
			mkdir $HOME/Desktop
		fi
		# make a copy of all files from Bureau to Desktop
		cp -n $HOME/Bureau/*.*  $HOME/Desktop/.

		sleep 10
		gnome-session-quit --logout --force
		#statements
	else
		echo " no change requested , exiting "
		exit 0
	fi

else

	echo     " la langue du bureau est anglais  (desktop language is english ) "
	echo     " voulez vous changer pour le francais (do you want to switch to french) ? "
	read  -p "                                                      your choice (y/n) : "  l_input
	l_input="${l_input:-"n"}"
	if [ $l_input = "y" ] ; then
		sudo /usr/sbin/update-locale LANG=fr_FR.UTF-8 LC_ALL=fr_FR.UTF-8 LANGUAGE=fr_FR
		echo "*******************************************************************"
		echo "*****  forced logout in 10  secondes                          *****"
		echo "*****                                                         *****"
		echo "*****  IMPORTANT : Please read                                *****"
		echo "*****                                                         *****"
		echo "*****  when relogging, answer YES to directory names update   *****"
		echo "*****                                                         *****"
		echo "*****  IMPORTANT                                              *****"
		echo "*******************************************************************"
		## it is important to bear in mind that directory name will change and especially Bureau will become Desktop
		if [ ! -d "$HOME/Bureau" ]; then
			# desktop directory does not exist, create it 
			mkdir $HOME/Bureau
		fi
		# make a copy of all files from Bureau to Desktop
		cp -n $HOME/Desktop/*.*  $HOME/Bureau/.
		sleep 5
		gnome-session-quit --logout --force
		#statements
	else
		echo " no change requested , exiting "
		exit 0
	fi

	#statements
fi





# laas_version=$(curl -s --request GET --header 'PRIVATE-TOKEN: Yioqks7dM4smYeSQc1W8' 'https://gitlab.forge.orange-labs.fr/devops-store/AtaqTools/raw/master/laas_docker_version.json') 


# laas_version_string_size=$(echo $laas_version | wc -c )



# if [ laas_version != .*laas.*laas.*  ]; then
# 	#statements
# 	echo "there is an issue with devops store access : "
# 	echo $laas_version
# 	sleep 2
# 	droydversion="1.1"
# 	appiumversion="1.1"
# 	read  -p "Please enter droydrunner version (default : $droydversion) : "  l_input
# 	droydversion="${l_input:-$droydversion}"

# else
# 	# jq is a dedicated tool which parses json string , finally, strip the "
# 	droydversion=$(echo $laas_version | jq '.laas_droydrunner' | sed -e 's/"//g')
# 	appiumversion=$(echo $laas_version | jq '.laas_appium'  | sed -e 's/"//g')
# fi


# droydURL="dockerfactory-iva.si.francetelecom.fr/laas_droydrunner:$droydversion"
# appiumURL="dockerfactory-iva.si.francetelecom.fr/laas_appium:$appiumversion"

# echo "droyd URL : "$droydURL
# echo "appium URL : "$appiumURL

# sleep 2



# if docker ps -a --format '{{.Names}}' | grep -q 'droydrunner' ; then
# 	echo "\ncontainer droydrunner exists locally"
# 	echo "stop container if running .... and then delete it" 
# 	docker stop droydrunner >/dev/null
# 	docker rm droydrunner
# fi

# echo "\ndownload latest image version if needed ....."
# docker pull $droydURL
# echo "\nstart droydrunner container"
# echo "\n        container will start tcp port 5200 for remote Laas Library "
# echo "\n        container will start tcp port 5201 for droydrunner server entry point "
# docker run -d --name droydrunner -p 5200:5000 -p 5201:5001 --privileged -v /dev/bus/usb:/dev/bus/usb $droydURL
# # erase images which are no more associated with a container
# docker image prune -f



# echo "\n\nusefull docker related commands : "
# echo "    docker start <container name> " 
# echo "    docker stop  <container name> " 
# echo "    docker ps    (list running container) " 
# echo "    docker ps -a (list all containers running or not) " 