#!/bin/bash

# first , get the latest version number of both appium and droydrunner container

# IMPORTANT : the private TOKEN used to acces -READ ONLY- is belonging to ATAQ team 
# -s option is mandatory to avoid a download 'progress' status which would mess a ittle bit the script output

laas_version=$(curl -s --request GET --header 'PRIVATE-TOKEN: Yioqks7dM4smYeSQc1W8' 'https://gitlab.forge.orange-labs.fr/devops-store/AtaqTools/raw/master/laas_docker_version.json') 


laas_version_string_size=$(echo $laas_version | wc -c )



if [ laas_version != .*laas.*laas.*  ]; then
	#statements
	# echo "there is an issue with devops store access : "
	# echo $laas_version
	sleep 1
	droydversion="1.4"
	appiumversion="1.6"

	dialog --backtitle "DROYDRUNNER Version selection " --title "DroydRunner Version Selection - Form" \
    --form "use <tab> or <arrow> to move" 12 60 5 \
"DroydRunner Version (check DevopsStore):" 1 1 $droydversion  1 44 6 6  > /home/$USER/bin/form.tmp \
    2>&1 >/dev/tty

    exitcode=$?


    if [ $exitcode = 1 ] ; then
	    clear
	    echo " bye , have a nice day ... "
	    exit 0
    fi

    droydversion=`sed -n 1p /home/$USER/bin/form.tmp`

    if [[ ! $droydversion =~ [0-9].[0-9] ]] ; then 
    	clear
		echo "Droydrunner version invalid"
		echo "it should be <digit>.<digit>"
		echo "bye"
		exit 0	
	fi 


	# read  -p "Please check devops store to enter last available droydrunner version (default : $droydversion) : " l_input
	# droydversion="${l_input:-$droydversion}"
	

else
	# jq is a dedicated tool which parses json string , finally, strip the "
	droydversion=$(echo $laas_version | jq '.laas_droydrunner' | sed -e 's/"//g')
	appiumversion=$(echo $laas_version | jq '.laas_appium'  | sed -e 's/"//g')
fi

clear

droydURL="dockerfactory-cwfr1.rd.francetelecom.fr/laas_droydrunner:$droydversion"
appiumURL="dockerfactory-cwfr1.rd.francetelecom.fr/laas_appium:$appiumversion"

echo " "
echo "droydrunner container URL : "$droydURL
# echo "appium URL : "$appiumURL
echo " "

sleep 1



if docker ps -a --format '{{.Names}}' | grep -q 'droydrunner' ; then
	echo "container droydrunner exists locally"
	echo "stop container if running .... and then delete it" 
	docker stop droydrunner >/dev/null
	docker rm droydrunner
fi

echo "download latest image version if needed ....."
docker pull $droydURL
echo "start droydrunner container"
echo "        container will start tcp port 5000 for remote Laas Library "
echo "        container will start tcp port 5001 for droydrunner server entry point "
docker run -d --name droydrunner -p 5000:5000 -p 5001:5001 --privileged -v /dev/bus/usb:/dev/bus/usb $droydURL
# erase images which are no more associated with a container
docker image prune -f



if docker ps --format '{{.Names}}' | grep -q 'droydrunner' ; then

	    dialog --colors  --title "DROYDRUNNER SETUP" --msgbox "  \Z2!!  CONGRATULATIONS  !!\Zn \n\
 DROYDRUNNER container is now running  \n\n\
  - tcp port \Zu5000\Zn is used to access remote Laas Library \n\
  - tcp port \Zu5001\Zn is used as droydrunner server entry point \n\n\
                   " 12 90
	# echo "***************************************************"
	# echo "***************************************************"
	# echo "****             CONGRATULATIONS              *****"
	# echo "****  container droydrunner is now running    *****"
	# echo "***************************************************"
	# echo "***************************************************"
else

	dialog --title "DROYDRUNNER SETUP" --msgbox "  \Z1!!  WARNING  !! \Zn \n\
 DROYDRUNNER container launch has failed  \n\n\
  - the root cause may come from already used ports  \n\
   or it may come from a wrong version number\n\
                   " 12 90
	# echo "********************************************************"
	# echo "********************************************************"
	# echo "****             WARNING                           *****"
	# echo "****    container droydrunner launch has failed    *****"
	# echo "********************************************************"
	# echo "********************************************************"
	echo " netstat results   "
	sudo netstat -tunlep | grep LISTEN | awk '{print $4  "     "  $9}'
	exit 0

fi



	    whiptail --title "Docker Commands" --msgbox "INFORMATION : usefull docker related commands : \n\
    docker start <container name>\n\
    docker stop <container name>\n\
    docker ps    (list running container)\n\
    docker ps -a (list all containers running or not)\n\n\
    docker exec -it <container name> <command> \n\
    but you can also use 'portainer' which is very simple\n\
	   " 14 70


# echo "\n\nusefull docker related commands : "
# echo "    docker start <container name> " 
# echo "    docker stop  <container name> " 
# echo "    docker ps    (list running container) " 
# echo "    docker ps -a (list all containers running or not) " 



















# laas_version=$(curl -s --request GET --header 'PRIVATE-TOKEN: Yioqks7dM4smYeSQc1W8' 'https://gitlab.forge.orange-labs.fr/devops-store/AtaqTools/raw/master/laas_docker_version.json') 


# laas_version_string_size=$(echo $laas_version | wc -c )



# if [ laas_version != .*laas.*laas.*  ]; then
# 	#statements
# 	echo "there is an issue with devops store access : "
# 	echo $laas_version
# 	sleep 1
# 	droydversion="1.2"
# 	appiumversion="1.3"
# 	read  -p "Please check devops store to enter last available droydrunner version (default : $droydversion) : " l_input
# 	droydversion="${l_input:-$droydversion}"
	

# else
# 	# jq is a dedicated tool which parses json string , finally, strip the "
# 	droydversion=$(echo $laas_version | jq '.laas_droydrunner' | sed -e 's/"//g')
# 	appiumversion=$(echo $laas_version | jq '.laas_appium'  | sed -e 's/"//g')
# fi


# droydURL="dockerfactory-cwfr1.rd.francetelecom.fr/laas_droydrunner:$droydversion"
# appiumURL="dockerfactory-cwfr1.rd.francetelecom.fr/laas_appium:$appiumversion"

# echo "\n"
# echo "droydrunner container URL : "$droydURL
# # echo "appium URL : "$appiumURL
# echo "\n"

# sleep 1



# if docker ps -a --format '{{.Names}}' | grep -q 'droydrunner' ; then
# 	echo "\ncontainer droydrunner exists locally"
# 	echo "stop container if running .... and then delete it" 
# 	docker stop droydrunner >/dev/null
# 	docker rm droydrunner
# fi

# echo "\ndownload latest image version if needed ....."
# docker pull $droydURL
# echo "\nstart droydrunner container"
# echo "\n        container will start tcp port 5000 for remote Laas Library "
# echo "\n        container will start tcp port 5001 for droydrunner server entry point "
# docker run -d --name droydrunner -p 5000:5000 -p 5001:5001 --privileged -v /dev/bus/usb:/dev/bus/usb $droydURL
# # erase images which are no more associated with a container
# docker image prune -f



# if docker ps --format '{{.Names}}' | grep -q 'droydrunner' ; then
# 	echo "***************************************************"
# 	echo "***************************************************"
# 	echo "****             CONGRATULATIONS              *****"
# 	echo "****  container droydrunner is now running    *****"
# 	echo "***************************************************"
# 	echo "***************************************************"
# else
# 	echo "********************************************************"
# 	echo "********************************************************"
# 	echo "****             WARNING                           *****"
# 	echo "****    container droydrunner launch has failed    *****"
# 	echo "********************************************************"
# 	echo "********************************************************"
# 	echo " the root cause may come from already used ports   "
# 	sudo netstat -tunlep | grep LISTEN | awk '{print $4  "     "  $9}'
# fi




# echo "\n\nusefull docker related commands : "
# echo "    docker start <container name> " 
# echo "    docker stop  <container name> " 
# echo "    docker ps    (list running container) " 
# echo "    docker ps -a (list all containers running or not) " 