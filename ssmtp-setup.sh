#!/bin/bash

# change language of the bureau 


#  https://www.ubuntu-user.com/Magazine/Archive/2014/21/Create-menus-and-dialogs-for-shell-scripts/(offset)/2





echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****    sSMTP setup with your GMAIL account setup     *****"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"


dialog --colors --title "sSMTP CLIENT SETUP" --yesno "This is the setup program to configure the sSMTP CLIENT to send mail with shell CLI \n\n\
This SETUP program will let you send mail with 'mail' or 'mutt' program \n\
exemple : \n\
\Z4echo 'hello, content of the message' | mail -s 'subject of the mail' <your mail>@orange.com\Zn\n\n\
This is very useful if you write shell script or launch Jenkins Job which must send mail \n\n\
You may use whatever free SMTP server in the WEB but the software has been pre configured to use Gmail authenticated smtp server :\n\
    - smtp.gmail.com with port 587 \n\
Finalizing the configuration requires to set : \n\
   - \Z2a gmail login :\Zn\n\
   - \Z2a gmail password :\Zn\n\
2 different files will be modified : \n\
   - \Z2/etc/smtp/ssmtp.conf\Zn \n\
   - \Z2/etc/smtp/revaliases\Zn \n\
Do you want to go on ?\n\
                  " 26 90

if [ $? = 1 ] ; then
	clear
	echo " bye , have a nice day ... "
	exit 0
fi

dialog --colors --title "sSMTP CLIENT SETUP" --yesno "Few security recommendations :\n\n\
\Z1Create a dedicated gmail account for this mail forwarding purpose \n\
because you will have to decrease the security level of your account to grant access to 'mail' application\Zn \n\
Do not use your Gmail configured for your android phone for exemple\n\
Basically, you can create new Gmail account like 'sendmail.<your_Name>@gmail.com'\n\n\
\Z1WARNING:\Zn \n\
Do not use '\' in your password, it seems to create issues, you may use '-' as symbol instead \n\
For security reasons, Gmail will also refuse first connection attempts from an unknown computer\n\
So create your new dedicated Gmail account with your 'live' or connect to your Gmail account with your 'live' first\n\
Do you want to go on ?\n\
                  " 22 90

sleep 1

Current_User=$(sudo grep AuthUser /etc/ssmtp/ssmtp.conf | sed -e 's/.*=//')
Current_Pwd=$(sudo grep AuthPass /etc/ssmtp/ssmtp.conf | sed -e 's/.*=//')

dialog --backtitle "sSMTP Gmail client Setup " --title "Gmail Credentials" \
--form "use <tab> and <arrow> to move" 12 55 4 \
"Username:" 1 1 "$Current_User"  1 12 35 35  \
"Password:" 2 1 "$Current_Pwd"   2 12 35 35  > /home/$USER/bin/form.tmp \
2>&1 >/dev/tty



exitcode=$?

clear

if [ $exitcode = 1 ] ; then
	clear
	echo " bye , have a nice day ... "
	exit 0
fi

GMAIL_User=`sed -n 1p /home/$USER/bin/form.tmp`
GMAIL_Password=`sed -n 2p /home/$USER/bin/form.tmp`


if [ -z "$GMAIL_User" ]  ; then
	echo "GMAIL user not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

if [ -z "$GMAIL_Password" ]  ; then
	echo "GMAIL Password not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi


sudo sed -i "s/AuthUser=.*/AuthUser=$GMAIL_User/g" /etc/ssmtp/ssmtp.conf
sudo sed -i "s/AuthPass=.*/AuthPass=$GMAIL_Password/g" /etc/ssmtp/ssmtp.conf


sudo sed -i "s/^root:.*/root:$GMAIL_User:smtp.gmail.com:587/g" /etc/ssmtp/revaliases
sudo sed -i "s/^$USER:.*/$USER:$GMAIL_User:smtp.gmail.com:587/g" /etc/ssmtp/revaliases

dialog --title 'sSMTP CLIENT SETUP' --colors --msgbox "\
\Z2!! Congratulations, ssmtp is now configured !!\Zn\n\n\
You may now test the configuration with a simple command :  \n\
Just type \Z4 echo 'message text' | mail -s 'message topic' <your orange mail> \Zn\n\
\ZnYou may check \Z4/var/log/mail.log\Zn in case of failure\n\
Most of the time, failure are related to authentication, be sure to have granted access to lower security equipments in your gmail account\n\
Type <CR> to exit\n\n\
                   " 20 80

echo " "
echo " "




