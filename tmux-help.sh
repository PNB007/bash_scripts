#!/bin/bash

# change language of the bureau 




echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****                 TMUX Help                        *****"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"


dialog --colors --title "TMUX Helper" --msgbox "TMUX has already been installed and is a very convenient Terminal Multiplexer :\n\n\
You can manage three layers of 'objects' : \n\
    - \Z2sessions\Zn , it is indeed the Linux terminal which is running TMUX\n\
      you can launch several sessions in different Linux Terminal though it is not much useful\n\

    - \Z2windows\Zn : once TMUX is running inside a linux terminal, you can create several windows and move from one window to the other\n\
      only one window is visible at a time\n\

    - \Z2pane\Zn : rather weird 'wording', you are able to split a window (or panes) into 'panes' with a horizontal or vertical split\n\
      this is very convenient if you have a 'large' window and wants to see several processes simultaneously\n\
Next, you will learn how to manage sessions\n\
                  " 22 90


dialog --colors --title "TMUX Helper" --msgbox "Session Management :\n\n\
Create a new session :  \n\
    - \Z2tmux \Zn in a linux terminal \n\
    - \Z2tmux new -s <myname>\Zn : in a linux terminal, it creates a new session with a name\n\
dettach from a session : \n\
    - \Z2Ctrl-b d \Zn in a session \n\
      it does not delete the session but let it run in background \n\
list existing sessions : \n\
    - \Z2tmux ls\Zn : in a linux terminal or in a session \n\
reattach a session :\n\
    - \Z2tmux a -t <myname or session number>\Zn : in a linux terminal \n\
      Note : attaching a session while you are already in a session is hazardeous \n\
kill a session : \n\
    - \Z2 exit all windows inside a session\Zn or \n\
    - \Z2 tmux kill-session -t <myname or session number>\Zn : in a linux terminal \n\
                  " 26 90

dialog --colors --title "TMUX Helper" --msgbox "Windows Management :\n\n\
Inside a session, you can create as many windows as you want, you will see the list of windows in the bottom of the terminal\n\
Generally, inside a TMUX session, all commands start with \Z2Ctrl-b\Zn followed by a key\n\
Create a new window :  \n\
    - \Z2Ctrl-b c \Zn in a Tmux session \n\
Jump to a window :  \n\
    - \Z2Ctrl-b <number> \Zn in a Tmux session \n\
    or \n\
    - \Z2Ctrl-b w \Zn in a Tmux session to list existing windows and select with <arrow> \n\
    or \n\
    - \Z2Ctrl-b n \Zn  to move to next window \n\
Rename a window : \n\
    - \Z2tmux rename-window <new name>\Zn inside a window \n\
delete a window : \n\
    - \Z2Ctrl-b & \Zn in a Tmux session \n\
    or \n\
    - \Z2simply exit \Zn \n\
                  " 24 90



dialog --colors --title "TMUX Helper" --msgbox "Panes Management :\n\n\
Inside a window, you can split it in 'panes' \n\
Split a window (or a pane) horizontally  :  \n\
    - \Z2Ctrl-b \" or h \Zn in a window (or a pane) \n\
Split a window (or a pane) vertically  :  \n\
    - \Z2Ctrl-b % or v\Zn in a window (or a pane) \n\
Move to next Pane :  \n\
    - \Z2Ctrl-b o \Zn in a pane \n\
Move to another pane with arrow : \n\
    - \Z2Ctrl-b <left, right, up or down arrow> \Zn in a pane \n\
    - \Z2Alt-<left, right, up or down arrow> \Zn in a pane \n\
    Note : the Alt-<arrow> is not a default configuration and has been set in ~/.tmux.conf \n\
Delete current Pane :  \n\
    - \Z2Ctrl-b x \Zn \n\
resize Pane with arrow : \n\
    - \Z2Ctrl-b-<left, right, up or down arrow> \Zn in a pane \n\
    note : type simultaneously Ctrl-b and arrow\n\
                  " 26 90


dialog --colors --title "TMUX Helper" --yesno "Panes Management :\n\n\
\Z1WARNING : \Zn it is not possible to go up/down in a window or in a pane with arrow or mouse\n\
you must switch to 'copy mode' wth a specific key : \n\
    - \Z2Ctrl-b [ or e \Zn to enter 'copy mode' to be able to move upwards or backwards with <arrow> <page up or down>\n\
    - \Z2q \Zn to quit the copy mode and go back to the prompt\n\n\
\Z2INFORMATION: \Zn You can customize TMUX and change 'key binding' in a file : \Z2.tmux.conf\Zn in your home directory\n\
Be cautious and Enjoy !!\n\
You can also launch tmux with $HOME/bin/tmux-launch.sh \n\
It will automatically launch tmux with 2 windows and several panes \n\
It will be automatically launched after this 'help' \n\
There is a summary of commands in \Z2$HOME/bin/tmux-key.txt\Zn\n\
Do you want to open this file ? \n\
                  " 24 90

response=$?

case $response in
  1 | 255)
    clear
    echo " you have typed no or esc , have a nice day ... " ;;
  0) 
    clear
    printf " Wait for sublime to open the file \n "
    sublime $HOME/bin/tmux-key.txt
esac


$HOME/bin/tmux-launch.sh


