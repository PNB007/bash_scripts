#!/bin/bash

# change language of the bureau 




echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****       RobotFramework Project setup               *****"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"


dialog --colors --title "Robot Project Setup" --yesno "This setup program creates a Robot Project with all directories and libraries \n\n\
The project will be created in \Z2$HOME/Robot_Project\Zn \n\
3 main parameters must be configured : \n\n\
 - Project NAME\n\
 - TestSuite NAME \n\
 - TestCase NAME\n\n\
 Do you want to go on ?\n\
                  " 18 90

if [ $? = 1 ] ; then
	clear
	echo " bye , have a nice day ... "
	exit 0
fi

sleep 1

echo "launching form "




dialog --backtitle "Project Setup " --title "Robot Project Setup - Form" \
--form "use <tab> or <arrow> to move" 12 60 0 \
"Project   Name:" 1 1 "Name"  1 20 20 20  \
"TestSuite Name:" 2 1 "TestSuite" 2 20 20 20  \
"TestCase  Name:" 3 1  "TestCase" 3 20 20 20  > /home/$USER/bin/form.tmp \
2>&1 >/dev/tty

exitcode=$?

clear

if [ $exitcode = 1 ] ; then
	clear
	echo " bye , have a nice day ... "
	exit 0
fi

Project=`sed -n 1p /home/$USER/bin/form.tmp`
TestSuite=`sed -n 2p /home/$USER/bin/form.tmp`
TestCase=`sed -n 3p /home/$USER/bin/form.tmp`

if [ -z "$Project" ]  ; then
	echo "Project Name not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

if [ -z "$TestSuite" ]  ; then
	echo "TestSuite Name not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

if [ -z "$TestCase" ]  ; then
	echo "TestCase Name not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

# substitute a whitespace with a _
Project="${Project// /_}"
TestSuite="${TestSuite// /_}"
TestCase="${TestCase// /_}"



echo "Creating project ressources ...."

echo $Project
echo $TestSuite
echo $TestCase

sleep 1

cd $HOME/Robot_Project

python3 /usr/local/bin/Structure_RF.py $Project $TestSuite $TestCase





