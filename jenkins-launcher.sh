#!/bin/bash

# change language of the bureau 




file="$HOME/bin/jenkins-first-launch.txt"
if ! [ -f "$file" ]
then



	echo "************************************************************"
    echo "************************************************************"
    echo "*****                                                  *****"
    echo "*****                                                  *****"
    echo "*****            Jenkins launcher                      *****"
    echo "*****                                                  *****"
    echo "*****                                                  *****"
    echo "************************************************************"
    echo "************************************************************"
	echo     "         "
	echo     "         "
	sleep 1

    echo "launching setup "

    AdminPassword=$(cat /var/lib/jenkins/secrets/initialAdminPassword)
    # it was impossible to change color of he text inside the window for part of the message
    #echo $tmpAdminPassword
    # tmpAdminPassword+=$'\e[0m'
    # AdminPassword=$'\e[48;5;196m'
    # AdminPassword+=$tmpAdminPassword
    
    echo $AdminPassword  | xclip -selection clipboard

# note : it s a mess , whiptail lets you select and copy but you cant set different colors for a text in the same window
# dialog lets you set whatever color you want but you can not select and copy ....

# export NEWT_COLORS='
# textbox=red,white
# '

dialog --title 'JENKINS INITIAL SETUP' --colors --msgbox "\
  \Z1!! WARNING !!\Zn\n\
 During initial Setup, you will be asked to unlock Jenkins with the following 'Administrator Password' :\n\n\
           \Z1$AdminPassword\Zn\n\n\
 This Password has \Z2already\Zn been put automatically into the 'clipboard' \n\n\
 Just type \Z2CTRL-V (or <mouse right-click> paste)\Zn when you will be asked to give it during Jenkins Setup\n\
 Type <CR> to go on\n\n\
                   " 20 80
 


  aptcacher=$(cat ~/bin/aptcacher.txt)
  if [ ! $aptcacher = "noneed" ]; then

     dialog --title "JENKINS INITIAL SETUP" --colors --msgbox "\n\
 Proxy settings :\n\
 If you use CNTLM, which is the default configuration in the RSC, \n\
 the initial Setup will ask for 'proxy settings' which are : \n\
    \Z2Server\Zn : 127.0.0.1\n\
    \Z2Port\Zn   : 3128\n\n\
    \Z2note\Zn : no user, no password and no proxy host are needed \n\n\
 Type <CR> to go on\n\n\
                   " 16 80

  fi

 
    dialog --title "JENKINS SETUP" --colors --msgbox "\Z1WARNING\Zn : 'JENKINS gives blank page' issue : \n\
 
 Once everything is properly initialized, you may sometimes hit the 'Jenkins blank page' issue when you log in\n\
 In such situation, just restart the service : \n\
  - \Z2sudo service jenkins restart\Zn \n\
    or \n\
  - \Z2http://localhost:8080/restart\Zn \n\
  It should fix the issue \n\
  Note : 'restart' may also be used in any other failure situation\n\
                   " 18 80

  touch $file

else
  echo "Jenkins initial Setup already done"
fi


clear

echo "wait for firefox to open Jenkins Home page ....."

# launch firefox with nohup or the kill command will kill also all sons too 
nohup firefox http://localhost:8080  > /dev/null 2>&1  &

# kill the parent process which is the window 
# kill -s TERM $PPID
exit 0











