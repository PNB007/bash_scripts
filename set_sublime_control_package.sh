#! /bin/bash

# following script launches sublime , then waits for 4 sec and then kills the sublime process
# so sublime has now set its own config files and it is possible to copy the package cotrol file to the right directory



if ! [ -f "/home/$USER/.config/sublime-text-3/Installed\ Packages/Package\ Control.sublime-package" ]
then


	echo "************************************************************"
	echo "************************************************************"
	echo "*****                                                  *****"
	echo "*****                                                  *****"
	echo "*****    Sublime text Robot Pluggin setup              *****"
	echo "*****                                                  *****"
	echo "*****                                                  *****"
	echo "************************************************************"
	echo "************************************************************"


	dialog --colors --title "Sublime Robot Pluggin SETUP" --yesno "This is the setup program helping \Z4\ZuRobotFramework pluggin for Sublime text\Zn installation\n\n\
	This SETUP program will help you to configure sublime text editor to understand \Z2'.robot'\Zn files  \n\
	With the pluggin, .robot files are much easier to read  with \Z2'colors'\Zn \n\
	RED is thus not always mandatory \n\
	Do you want to go on ?\n\
	                  " 12 90

	if [ $? = 1 ] ; then
		clear
		echo " bye , have a nice day ... "
		exit 0
	fi



	sublime
	sleep 2
	l_pid=$(ps -aux | grep ' [s]ublime' | awk '{print $2}')
	kill $l_pid
	cp /home/$USER/bin/Package\ Control.sublime-package /home/$USER/.config/sublime-text-3/Installed\ Packages/Package\ Control.sublime-package
	sleep 2
	sublime  /home/$USER/bin/sublime_help.txt 

	# modify config file : cant be done before package is installed ...

	# sed -i 's/"robot_framework_workspace":.*/"robot_framework_workspace": "home\/$USER\/Robot_Project" ,/g' /home/$USER/.config/sublime-text-3/Packages/RobotFrameworkAssistant/Robot.sublime-settings

	dialog --colors --title "Sublime Robot Pluggin SETUP" --msgbox "Sublime 'Package Control' has been successfully installed but there are two more steps to do : \n\
		- manually install the 'Robot' pluggin  \n\
		- manually configure robot pluggin default settings \n\
	Just have a look at the 'help' file which has been opened by sublime \n\
	Enjoy !\n\
	                  " 12 90

	clear

else
	sublime  /home/$USER/bin/sublime_help.txt 
fi


