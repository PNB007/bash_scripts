
#! /bin/bash


# foreground colors 
FGblue="\e[38;5;17m"
FGgreen="\e[38;5;40m"
FGpink="\e[38;5;165m"
FGwhite="\e[38;5;255m"
FGred="\e[38;5;196m"
#background colors
BGgreen="\e[48;5;40m"
BGred="\e[48;5;196m"
BGwhite="\e[48;5;255m"
BGgrey="\e[48;5;232m"
razcolor='\e[0m'

clear


printf "exemple of a simple box with a single color inside \n"
printf $FGgreen
printf $BGgrey
echo "whiptail --title 'Message' --msgbox 'Hello, world! in a single color ' 8 25"
printf $razcolor
read -p "type <CR> to go on " uselessvar


whiptail --title 'Message' --msgbox 'Hello, world! in a single color' 8 25

clear

printf "exemple of a simple question box with a single color inside \n"

printf $FGgreen
printf $BGgrey
echo "whiptail --title \"Message\"  --yesno \"Are you having fun? in a single color \" 15 25 "
printf $razcolor

read -p "type <CR> to go on " uselessvar

whiptail --title "Message"  --yesno "Are you having fun? in a single color" 15 25

response=$?

case $response in
	1 | 255)
    clear
	echo " you have typed no or esc , have a nice day ... "
	exit 0;;
	0) 
    clear
    printf " you have typed $FGpink $BGgrey yes $razcolor, we go on \n" ;;
esac

read -p "type <CR> to go on " uselessvar

clear


echo "exemple of a SINGLE choice in a list "

printf $FGgreen
printf $BGgrey
echo "whiptail --radiolist \"Choose one colour :\" 15 40 5 \\"
echo "\"first\" \"red\" ON \\"
echo "\"second\" \"green\" OFF \\"
echo "\"third\" \"blue\" OFF \\"
echo "\"any label\" \"any color\" OFF 2> tmpinput "
printf $razcolor

whiptail --radiolist "Choose one colour :" 15 40 5 \
"first" "red" ON \
"second" "green" OFF \
"third" "blue" OFF \
 "any label" "any color" OFF 2> tmpinput

response=$?

case $response in
	1 | 255)
    clear
	echo " you have typed no or esc , have a nice day ... "
	exit 0;;
	0) 
    clear
    my_choice=$(cat tmpinput)
    printf " you have chosen $FGpink $BGgrey $my_choice  $razcolor, we go on \n " ;;
esac

read -p "type <CR> to go on " uselessvar

clear


echo "exemple of MULTIPLE choice in a list "

printf $FGgreen
printf $BGgrey
echo "whiptail --checklist \"Choose MULTIPLE colours :\" 15 40 5 \\"
echo "\"first\" \"red\" ON \\"
echo "\"second\" \"green\" OFF \\"
echo "\"third\" \"blue\" OFF \\"
echo "\"any label\" \"any color\" OFF 2> tmpinput "
printf $razcolor

whiptail --checklist "Choose MULTIPLE colour :" 15 40 5 \
"first" "red" ON \
"second" "green" OFF \
"third" "blue" OFF \
 "any label" "any color" OFF 2> tmpinput

response=$?

case $response in
	1 | 255)
    clear
	echo " you have typed no or esc , have a nice day ... "
	exit 0;;
	0) 
    clear
    my_choice=$(cat tmpinput)
    printf " you have chosen $FGpink $BGgrey $my_choice  $razcolor, we go on \n"
    printf "$FGred WARNING $razcolor, choices are surrounded by \" \n" 
    echo " print results as items in a list using for statement "
    for my_item in $my_choice
    do
        printf " $FGpink $BGgrey $my_item $razcolor \n"
    done

esac

read -p "type <CR> to go on " uselessvar

clear

echo "exemple of a password input "
printf $FGgreen
printf $BGgrey
echo "whiptail --passwordbox \"your password\" 10 40 \\"
echo "       2> tmpinput"
printf $razcolor

read -p "type <CR> to go on " uselessvar

# exemple of an nput to get a passwd 
whiptail --passwordbox "your password" 10 40  \
       2> tmpinput

response=$?

case $response in
	1 | 255)
    clear
	echo " you have typed no or esc , have a nice day ... "
	exit 0;;
	0) 
    clear
    my_passwd=$(cat tmpinput)
    printf " your passwd : $FGpink $BGgrey $my_passwd $razcolor, we go on \n" ;;
esac

read -p "type <CR> to go on " uselessvar

clear

echo "open a file in a box , this /etc/newt/palette holds colors configuration "
echo "whiptail --scrolltext --title \"/etc/newt/palette file \" --textbox \"/etc/newt/palette\" 12 80"


whiptail --scrolltext --title "/etc/newt/palette file " --textbox "/etc/newt/palette" 12 80


clear

read -p "type <CR> to go on " uselessvar

printf $FGred
echo "WARNING : there is NO multiple input form with whiptail "
printf $razcolor


read -p "type <CR> to go on " uselessvar

whiptail --title 'Whiptail overview' \
--msgbox "this is the end of this quick overview of whitail which helps to create nice shell scripts \n\
What you should remember is that :\n\
- you cannot set differnt colors inside the text box \n\
- you do not have a multiple input form\n\
but you can : \n\
-select inside the box \n\
-use the password box which is better than with dialog\n\
Do not hesitate to google to get additional informations and enjoy \n\
" 18 70



